const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../database/connection');

// Se crea una constante corta para la conexion a la base de datos
const sequelize = dataBaseConnection;

// Modelo de datos para que haga match con los resultados que arroje
class tblIncidencias extends Model {
    idIncidencia;
    vchDescripcion;
    blnActivo;
}

// Se inicializan los camos con los tipos de datos tal cual está en la base de datos
tblIncidencias.init({
    idIncidencia: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    vchDescripcion: {
        type: DataTypes.STRING
    },
    blnActivo: {
        type: DataTypes.BOOLEAN
    }
}, {
    tableName: 'tblIncidencias_tickets',
    sequelize
});

// Se exportan los modulos
module.exports = tblIncidencias;