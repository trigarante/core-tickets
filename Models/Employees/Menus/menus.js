const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../../../database/connection');
const sequelize = dataBaseConnection;

class tblMenus extends Model { };

tblMenus.init({}, {
    tableName: 'tblMenus_tickets',
    sequelize
});

module.exports = tblMenus;