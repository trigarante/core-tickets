const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../../../database/connection');
const sequelize = dataBaseConnection;

class tblEmpleados extends Model {
    idEmpleado;
    vchNombre;
    vchApellidoPaterno;
    vchApellidoMaterno;
    blnActivo;
    fkIdTipoUsuario;
}

tblEmpleados.init({
    idEmpleado: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    vchNombre: {
        type: DataTypes.STRING
    },
    vchApellidoPaterno: {
        type: DataTypes.STRING
    },
    vchApellidoMaterno: {
        type: DataTypes.STRING
    },
    blnActivo: {
        type: DataTypes.BOOLEAN
    },
    fkIdTipoUsuario: {
        type: DataTypes.INTEGER
    }
}, {
    tableName: 'tblEmpleados_tickets',
    sequelize
});

module.exports = tblEmpleados;