const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../../../database/connection')
const sequelize = dataBaseConnection;

class viewMyTickets extends Model {
    idCreacionTicket;
    fechaSolicitud;
    idEstadoTicket;
    statusDescription;
    incidencesDescription;
    vchNombre;
    vchApellidoPaterno;
    vchApellidoMaterno;
    incidencesDetails;
    txtDescripcion;
    fkIdEmpleadoSolicita;
    fkIdEmpleadoAtiende;
}

viewMyTickets.init({
    idCreacionTicket: {
        type: DataTypes.INTEGER
    },
    fechaSolicitud: {
        type: DataTypes.DATE
    },
    idEstadoTicket: {
        type: DataTypes.INTEGER
    },
    statusDescription: {
        type: DataTypes.STRING
    },
    incidencesDescription: {
        type: DataTypes.STRING
    },
    vchNombre: {
        type: DataTypes.STRING
    },
    vchApellidoPaterno: {
        type: DataTypes.STRING
    },
    vchApellidoMaterno: {
        type: DataTypes.STRING
    },
    incidencesDetails: {
        type: DataTypes.STRING
    },
    txtDescripcion: {
        type: DataTypes.TEXT
    },
    fkIdEmpleadoSolicita: {
        type: DataTypes.INTEGER
    },
    fkIdEmpleadoAtiende: {
        type: DataTypes.INTEGER
    }
}, {
    tableName: 'viewMyTickets_tickets',
    sequelize
})

module.exports = viewMyTickets;