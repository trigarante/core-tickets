const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../database/connection');
const sequelize = dataBaseConnection;

/* class tblCreacionTickets extends Model {
    idCreacionTicket;
    txtDescripcion;
    blnActivo;
    fechaSolicitud;
    fkIdEstadoTicket;
    fkIdDetalleIncidencia;
    fkIdAreaAtencion;
    fkIdEmpleadoSolicita;
}; */

const tblCreacionTickets = sequelize.define('tblCreacionTickets', {
    idCreacionTicket: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    txtDescripcion: {
        type: DataTypes.TEXT
    },
    blnActivo: {
        type: DataTypes.BOOLEAN
    },
    fechaSolicitud: {
        type: DataTypes.DATE
    },
    fkIdEstadoTicket: {
        type: DataTypes.INTEGER
    },
    fkIdDetalleIncidencia: {
        type: DataTypes.INTEGER
    },
    fkIdAreaAtencion: {
        type: DataTypes.INTEGER
    },
    fkIdEmpleadoSolicita: {
        type: DataTypes.INTEGER
    },
    vchSedeSolicita: {
        type: DataTypes.STRING
    }
}, {
    tableName: 'tblCreacionTickets_tickets',
    sequelize
});

module.exports = tblCreacionTickets;