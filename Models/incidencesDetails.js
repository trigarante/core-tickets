const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../database/connection');

// Se crea una instancia corta para la conexión a la base de datos
const sequelize = dataBaseConnection;

class tblDetalleIncidencias extends Model {
    idDetalleIncidencia;
    vchDescripcion;
    blnActivo;
    fkIdIncidencia;
}

tblDetalleIncidencias.init({
    idDetalleIncidencia: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    vchDescripcion: {
        type: DataTypes.STRING
    },
    blnActivo: {
        type: DataTypes.BOOLEAN
    },
    fkIdIncidencia: {
        type: DataTypes.INTEGER
    }
}, {
    tableName: 'tblDetalleIncidencias_tickets',
    sequelize
});

module.exports = tblDetalleIncidencias;