const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../database/connection');
const sequelize = dataBaseConnection;

class tblSesiones extends Model {
    // idUsuario;
    // google;
    // email;
    // accessPass;
    // img;
}

tblSesiones.init({
    idUsuario: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    google: {
        type: DataTypes.BOOLEAN
    },
    email: {
        type: DataTypes.STRING
    },
    accessPass: {
        type: DataTypes.STRING
    },
    img: {
        type: DataTypes.STRING
    },
    nombre: {
        type: DataTypes.STRING
    }
}, {
    tableName: 'tblSesiones_tickets',
    sequelize,
    timestamps: false,
});

module.exports = tblSesiones;
