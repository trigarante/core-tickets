const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../../../database/connection');
const sequelize = dataBaseConnection;

class tblMensajesTickets extends Model {
    idMensajeTicket;
    fkIdCreacionTicket;
    jsonMensajes;
}

tblMensajesTickets.init({
    idMensajeTicket: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    fkIdCreacionTicket: {
        type: DataTypes.STRING
    },
    jsonMensajes: {
        type: DataTypes.JSON
    }
}, {
    tableName: 'tblMensajesTickets_tickets',
    sequelize
});

module.exports = tblMensajesTickets;