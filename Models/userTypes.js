const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../database/connection');

const sequelize = dataBaseConnection;

class tblTipoUsuarios extends Model {
    idTipoUsuario;
    vchDescripcion;
    blnActivo;
}

tblTipoUsuarios.init({
    idTipoUsuario: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    vchDescripcion: {
        type: DataTypes.STRING
    },
    blnActivo: {
        type: DataTypes.BOOLEAN
    }
}, {
    tableName: 'tblTipoUsuarios_tickets',
    sequelize
});

module.exports = tblTipoUsuarios;
