const { Model, DataTypes } = require('sequelize');
const { dataBaseConnection } = require('../database/connection');
const sequelize = dataBaseConnection;

class tblAreaAtencion extends Model {
    idAreaAtencion;
    vchDescripcion;
    blnActivo;
}

tblAreaAtencion.init({
    idAreaAtencion: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    vchDescripcion: {
        type: DataTypes.STRING
    },
    blnActivo: {
        type: DataTypes.BOOLEAN
    }
}, {
    tableName: 'tblAreaAtencion_tickets',
    sequelize
})

module.exports = tblAreaAtencion;