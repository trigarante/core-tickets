const { Router } = require('express');
const {
    getEmpleadosActivos,
    getEmployeeByEmail,
    getEmployeeByTicketId,
    getEmployeesByAtentionAreaID,
    getEmployeesByUserType,
    putStatusAccount,
    postNewEmployee,
    putUserType
} = require('../Controllers/Employees/Empleados/employees');
const { getMenusByEmployeeId } = require('../Controllers/Employees/Menus/menus');
const router = Router();

router.post('/', postNewEmployee);
router.get('/', getEmpleadosActivos);
router.get('/by-email/:email', getEmployeeByEmail);
router.get('/menus/:id', getMenusByEmployeeId)
router.get('/ticket-employee/:idTicket', getEmployeeByTicketId);
router.get('/by-atention-area/:areaID/:sede', getEmployeesByAtentionAreaID);
router.get('/get-employees-by-user-type/:userTypeID/:sede', getEmployeesByUserType);
router.put('/update-status-account/:empleadoID', putStatusAccount);
router.put('/update-user-type-id/:empleadoID', putUserType);

module.exports = router;