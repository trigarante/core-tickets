const { Router } = require('express');
const { getIncidenciasActivas } = require('../Controllers/incidences');
const router = Router();

// Urls para acceder a los métodos
router.get('/', getIncidenciasActivas);

module.exports = router;