const { Router } = require('express');
const { getDetalleIncidenciasActivas, getDetalleIncidenciasActivasById } = require('../Controllers/incidencesDetails');
const router = Router();

router.get('/', getDetalleIncidenciasActivas);
router.get('/:id', getDetalleIncidenciasActivasById);

module.exports = router;