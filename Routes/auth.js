/*
    Path: '/api/login'
*/
const { check } = require("express-validator");
const { validarCampos } = require("../middlewares/validar-campos");
const { postUsuariosLoginGoogle, renewToken, updateSessionActive } = require('../Controllers/auth');
const { validarJWT } = require('../middlewares/validar-jwt');
const { Router } = require('express');
const router = Router();
router.post('/google',
    postUsuariosLoginGoogle
)

router.get('/renew',
    validarJWT,
    renewToken
)

router.put('/session-active/:email', updateSessionActive);
module.exports = router;
