const { Router } = require('express');
const { postTicket } = require('../Controllers/tickets');
const { getViewTickets, getTicketsByStatusId, getTicketsNotAssigned, putTicketAreaAtencionEmpleadoAtiende, putNewStatusTicket } = require('../Controllers/tickets/views/viewTickets')
const router = Router();


router.get('/by-status/:sede/:status', getTicketsByStatusId);
router.get('/not-assigned/:sede', getTicketsNotAssigned);
router.get('/:id', getViewTickets);
router.post('/', postTicket);
router.put('/update-atention-info/:ticketID', putTicketAreaAtencionEmpleadoAtiende);
router.put('/update-state-ticket/:ticketID', putNewStatusTicket);

module.exports = router;