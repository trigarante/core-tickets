const { Router } = require('express');
const { getTipoUsuarios, getTipoUsuariosById } = require('../Controllers/userTypes');
const {validarJWT} = require('../middlewares/validar-jwt')
const router = Router();

router.get('/' /*validarJWT*/, getTipoUsuarios);
router.get('/:id', /*validarJWT*/ getTipoUsuariosById);

module.exports = router;
