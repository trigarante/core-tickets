const { Router } = require('express');
const { getEmployeesSupport } = require('../../Controllers/Permissions/permissions');
const router = Router();

router.get('/employees-support/:sede', getEmployeesSupport);

module.exports = router;