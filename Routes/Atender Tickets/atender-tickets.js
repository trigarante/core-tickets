const { Router } = require('express');
const { getTicketsAtencionById, putEstadoTicket, getAttendingEmployee, postEmployeeTicket } = require('../../Controllers/Atender Tickets/atender-tickets');
const router = Router();

router.get('/get-employee/:idTicket', getAttendingEmployee);
router.get('/:idEmpleado/:sede', getTicketsAtencionById);
router.post('/set-employee-ticket', postEmployeeTicket);
router.put('/', putEstadoTicket);
module.exports = router;
