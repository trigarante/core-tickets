const { Router } = require('express');
const { storage, downloader } = require('../../Helpers/multer');
const multer = require('multer');

const router = Router();

const uploader = multer({
    storage
}).single('file');

router.post('/upload', uploader, (req, res) => {
    const { body, file } = req;
    if (file && body) {
        res.json({ file })
    }
});

router.get('/download/:chat/:image', downloader)

module.exports = router;


