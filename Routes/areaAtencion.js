const {Router} = require('express');
const {getAreaAtencionActivos} = require('../Controllers/areaAtencion');
const router = Router();
router.get('/enable', getAreaAtencionActivos);
module.exports = router;