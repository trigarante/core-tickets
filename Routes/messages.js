const { Router } = require('express');
const { getMensajesById, postMensajes, putMensajes } = require('../Controllers/Messages/tables/messages');
const router = Router();

router.get('/:id', getMensajesById);
router.post('/', postMensajes);
router.put('/', putMensajes)

module.exports = router;