const { Router } = require('express');
const { getMenusAddedEmployee, getMenusNotAddedEmployee, putMenusStatusActive, postAddMenusToEmployee } = require('../../Controllers/Employees/Menus/menus');
const router = Router();

router.get('/added/:id', getMenusAddedEmployee);
router.get('/not-added/:id', getMenusNotAddedEmployee);
router.put('/update-menus/:id', putMenusStatusActive);
router.post('/insert-menus/:id', postAddMenusToEmployee);

module.exports = router;
