require('dotenv').config();
const express = require('express');
const path = require('path');
const cors = require('cors');
const io = require('socket.io');
// const { dbConnection } = require('./database/config');
const dbConnection = require('./database/connection');
const app = express();
app.use(cors());
// Lectura y parseo del body
app.use(express.json());
// Base de datos
dbConnection.dataBaseConnection;
//DIRECTORIO PUBLICO
app.use(express.static('Public'));
// Rutas
app.use('/api/usuarios', require('./Routes/usuarios'));
app.use('/api/login', require('./Routes/auth'));
app.use('/api/login/renew', require('./Routes/auth'));
// Urls para acceder a los metodos con la base de datos MySQL
app.use('/api/user-types', require('./Routes/userTypes'));
app.use('/api/incidences', require('./Routes/incidenses'));
app.use('/api/incidences-details', require('./Routes/incidencesDetails'));
app.use('/api/employees', require('./Routes/employees'));
app.use('/api/tickets', require('./Routes/tickets'));
app.use('/api/areas', require('./Routes/areaAtencion'));
app.use('/api/chat', require('./Routes/messages'));
app.use('/api/atender-ticket', require('./Routes/Atender Tickets/atender-tickets'));
app.use('/api/permissions', require('./Routes/Permissions/permissions'));
app.use('/api/menus', require('./Routes/Menus/menus'));

// Para consumir la imagen que se guarda en el servidor
app.use('/api/images', require('./Routes/Images/upload'));

// Lo último
app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'Public/index.html'));
});

const server = app.listen(process.env.PORT, () => {
    console.info('Servidor corriendo en puerto => ', process.env.PORT);
});

// Codigo para cuando el frontend esta corriendo en producción
const serviceIO = io(server, {
    cors: {
        origin: "https://ticketseguimiento.com",
        methods: ["GET", "POST"],
        allowedHeaders: ["socket-cors"],
        credentials: true
    }
});

// Descomentar este código si el frontend se está corriendo en local
/* const serviceIO = io(server, {
    cors: {
        origin: "http://localhost:4200",
        methods: ["GET", "POST"],
        allowedHeaders: ["socket-cors"],
        credentials: true
    }
}); */

serviceIO.sockets.on('connection', (socket) => {
    const idTicket = socket.handshake.query.token;
    socket.join(idTicket);
    socket.on('dataSocket', (data) => {
        socket.broadcast.to(`ticket_${data.ticketid}`).emit('broadcast', {
            message: data.message,
            profileId: data.profileId,
            profileName: data.profileName,
            dateSend: data.dateSend,
            timeSend: data.timeSend,
            image: data.image
        });
    });

    socket.on('notificacion', (data) => {
        serviceIO.sockets.emit('notificacion', {
            typenotification: data.typenotification,
            message: data.message,
            sender: data.sender,
            reciever: data.reciever,
            photo: data.photo,
            ticketid: data.ticketid,
            ticketstatus: data.ticketstatus
        });
    });

    // Socket para identificar el cambio de estado de un ticket
    socket.on('statusChange', (data) => {
        serviceIO.sockets.emit('statusChange', {
            newstatusticket: data.newstatus,
            ticketid: data.ticketid,
            sender: data.sender,
            receiver: data.receiver,
            statusticketdesc: data.statusticketdesc
        });
    })

});

