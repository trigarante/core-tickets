const viewMyTickets = require('../../../Models/tickets/views/viewTickets');
const { QueryTypes } = require('sequelize');
const fields = [
    'idCreacionTicket',
    'fechaSolicitud',
    'idEstadoTicket',
    'statusDescription',
    'incidencesDescription',
    'vchNombre',
    'vchApellidoPaterno',
    'vchApellidoMaterno',
    'incidencesDetails',
    'txtDescripcion',
    'fkIdEmpleadoSolicita',
    'fkIdEmpleadoAtiende'
];

const getViewTickets = async (req, res) => {
    const id = req.params.id;
    let viewTickets;
    try {
        viewMyTickets.findAll({
            attributes: fields,
            where: {
                fkIdEmpleadoSolicita: id
            }
        }).then(response => {
            viewTickets = response;
        }).catch(error => {
            console.error('Error al consultar vista de tickets ', error)
        }).finally(() => {
            console.error(viewTickets)
            res.json({ viewTickets })
        })
    } catch (error) {
        res.status(500).json({ error })
    }
}

const getTicketsByStatusId = async (req, res) => {
    try {
        const sede = req.params.sede;
        const status = req.params.status;
        let ticketsByStatus;
        await viewMyTickets.sequelize.query('CALL spSelectTicketsByStatus_tickets(:sede, :ticketStatus)', {
            replacements: {
                sede: sede,
                ticketStatus: status
            },
            type: QueryTypes.SELECT
        }).then(result => {
            ticketsByStatus = result;
        }).catch(error => {
            console.error('Error al consultar los tickets por estado => ', error);
        }).finally(() => {
            res.json({ ticketsByStatus });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const getTicketsNotAssigned = async (req, res) => {
    try {
        const sede = req.params.sede;
        let ticketsNotAssgined;
        await viewMyTickets.sequelize.query('CALL spSelectTicketsNotAttending_tickets(:sede)', {
            replacements: { sede: sede },
            type: QueryTypes.SELECT
        }).then(result => {
            ticketsNotAssgined = result;
        }).catch(error => {
            console.error('Error al consultar los tickets no atendidos => ', error);
        }).finally(() => {
            res.json({ ticketsNotAssgined });
        })
    } catch (error) {
        res.status(500).json(error);
    }
}


const putTicketAreaAtencionEmpleadoAtiende = async (req, res) => {
    try {
        const ticketID = req.params.ticketID;
        const params = req.body;
        let updatedTicket;
        await viewMyTickets.sequelize.query('CALL spUpdateEmployeeAndAreaByTicketID_tickets(:ticketID, :areaID, :empleadoID)', {
            replacements: {
                ticketID: ticketID,
                areaID: params.areaID,
                empleadoID: params.empleadoID
            },
            type: QueryTypes.UPDATE
        }).then(result => {
            updatedTicket = result;
        }).catch(error => {
            console.error('Error al actualizar los datos en la base de datos => ', error);
        }).finally(() => {
            res.json({
                success: true,
                body: updatedTicket
            });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const putNewStatusTicket = async (req, res) => {
    try {
        const ticketID = req.params.ticketID;
        const params = req.body;
        let updatedStatusTicket;
        await viewMyTickets.sequelize.query('CALL spUpdateStatusTicket_tickets(:ticketID, :status)', {
            replacements: {
                ticketID: ticketID,
                status: params.status
            },
            type: QueryTypes.UPDATE
        }).then(result => {
            updatedStatusTicket = result;
        }).catch(error => {
            console.error('Error al actualizar los datos en la base de datos => ', error);
            res.json({
                success: false
            });
        }).finally(() => {
            res.json({
                success: true
            });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    getViewTickets,
    getTicketsByStatusId,
    getTicketsNotAssigned,
    putTicketAreaAtencionEmpleadoAtiende,
    putNewStatusTicket
}