const tblIncidencias = require('../Models/incidences');

// En este array se definen los campos a consultar en la base de datos
const fields = ['idIncidencia', 'vchDescripcion', 'blnActivo'];

// Método para obtener las Incidencias que esten activas
const getIncidenciasActivas = async (req, res) => {
    try {
        // Variable donde se ha de almacenar el resultado de la base de datos
        let incidencias;
        // Método ara realizar la consulta de los registros
        tblIncidencias.findAll({
            attributes: fields,
            where: { blnActivo: true }
        }).then(response => {
            // Asigo a la varible local el resultado de la base de datos
            incidencias = response;
        }).catch(error => {
            // En caso de que haya error al consultar la base de datos
            console.error('Error al consultar los registros de Incidencias');
        }).finally(() => {
            // Al finalizar la petición se retorna la respuesta del backend
            res.json({ incidencias });
        });
    } catch (error) {
        // Error para indicar que falló el servidor
        res.status(500).json(error);
    }
};

module.exports = {
    getIncidenciasActivas
};