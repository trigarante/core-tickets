const tblAreaAtencion = require('../Models/areaAtencion');
const fields = ['idAreaAtencion', 'vchDescripcion'];

const getAreaAtencionActivos = async (req, res) => {
    try {
        let areasAtencion;
        tblAreaAtencion.findAll({
            attributes: fields,
            where: {
                blnActivo: true
            }
        }).then(resp => {
            areasAtencion = resp;
        }).catch(err => {
            console.error('Error al obtener los datos');
        }).finally( () => {
            res.json(areasAtencion);
        })
    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    getAreaAtencionActivos
}