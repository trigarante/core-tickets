const { QueryTypes } = require('sequelize');
const tblCreacionTicket = require('../../Models/Atender Tickets/atender-tickets');

const getTicketsAtencionById = async (req, res) => {
    const idEmpleado = req.params.idEmpleado;
    const sede = req.params.sede;
    try {
        let ticketAtendidos;
        tblCreacionTicket.sequelize.query('CALL spSelectTicketsAtendidosById_tickets(:idEmpleado, :sede)', {
            replacements: {
                idEmpleado: idEmpleado,
                sede: sede
            }, type: QueryTypes.SELECT
        }).then(resp => {
            ticketAtendidos = resp;
        }).catch(error => {
            console.error('Error al realizar consulta: ', error);
        }).finally(() => {
            res.json({ ticketAtendidos });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const putEstadoTicket = async (req, res) => {
    const params = req.body;
    try {
        const ticketActualizado = await tblCreacionTicket.sequelize
            .query('CALL spUpdateEstadoTicket_tickets(:idEstadoTicket,:idTicket)', {
                replacements: {
                    idEstadoTicket: params.estado,
                    idTicket: params.noTicket,
                }, type: QueryTypes.UPDATE
            }).then(resp => {
            }).catch(err => {
                console.error('Error al realizar consulta: ', error);
            }).finally(() => {
                console.info('Actualizacion Exitosa');
            });
        res.json({
            ok: true,
            ticketActualizado,
            msg: "Actualización Exitosa",
        })
    } catch (e) {
        res.status(500).json(e);
    }
}

const getAttendingEmployee = async (req, res) => {
    const idTicket = req.params.idTicket;
    try {
        let attendingEmployee;
        await tblCreacionTicket.sequelize.query('CALL spSelectAttendingEmployee_tickets(:ticketID);', {
            replacements: { ticketID: idTicket },
            type: QueryTypes.SELECT
        }).then(resp => {
            attendingEmployee = resp;
        }).catch(error => {
            console.error('Error al realizar consulta: ', error);
        }).finally(() => {
            res.json({ attendingEmployee });
        });
    } catch (e) {
        res.status(500).json(e);
    }
}

const postEmployeeTicket = async (req, res) => {
    try {
        const params = req.body;
        let employeeTicket;
        await tblCreacionTicket.sequelize.query('CALL spInsertOnTicketsAttending_tickets(:ticketID, :employeeID)', {
            replacements: { ticketID: params.ticketID, employeeID: params.employeeID },
            type: QueryTypes.INSERT
        }).then(response => {
            employeeTicket = response;
        }).catch(error => {
            console.error('Error al insertar el registro en la tabla => ', error);
        }).finally(() => {
            res.json({
                success: true,
                body: employeeTicket
            });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    getTicketsAtencionById,
    putEstadoTicket,
    getAttendingEmployee,
    postEmployeeTicket
};
