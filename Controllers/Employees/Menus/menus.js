const { QueryTypes } = require('sequelize');
const tblMenus = require('../../../Models/Employees/Menus/menus');

const getMenusByEmployeeId = async (req, res) => {
    const id = req.params.id;
    try {
        let menus;
        tblMenus.sequelize.query('CALL spSelectMenusByEmployeeId_tickets(:id)', {
            replacements: { id: id },
            type: QueryTypes.SELECT
        }).then(result => {
            menus = result;
        }).catch(error => {
            console.error('Error al consultar los menus en la base de datos => ', error);
        }).finally(() => {
            res.json({ menus });
        });
    } catch (error) {
        res.status(500).json(error);
    }
};

const getMenusAddedEmployee = async (req, res) => {
    try {
        const id = req.params.id;
        let menusAdded;
        tblMenus.sequelize.query('CALL spSelectMenusAddedByEmployeeId_tickets(:idEmpleado)', {
            replacements: { idEmpleado: id },
            type: QueryTypes.SELECT
        }).then(result => {
            menusAdded = result;
        }).catch(error => {
            console.error('Error al consultar los menus agregados => ', error);
        }).finally(() => {
            res.json({ menusAdded });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const getMenusNotAddedEmployee = async (req, res) => {
    try {
        const id = req.params.id;
        let menusNotAdded;
        tblMenus.sequelize.query('CALL spSelectMenusNotAddedByEmployeeId_tickets(:idEmpleado)', {
            replacements: { idEmpleado: id },
            type: QueryTypes.SELECT
        }).then(result => {
            menusNotAdded = result;
        }).catch(error => {
            console.error('Error al consultar los menus agregados => ', error);
        }).finally(() => {
            res.json({ menusNotAdded });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const putMenusStatusActive = async (req, res) => {
    try {
        const idEmployee = req.params.id;
        const status = req.body.status;
        const idMenu = req.body.idMenu;
        await tblMenus.sequelize.query('CALL spUpdateEmployeeHasMenuByEmployeeMenuId_tickets(:status, :idEmpleado, :idMenu)', {
            replacements: {
                status: status,
                idEmpleado: idEmployee,
                idMenu: idMenu
            },
            type: QueryTypes.UPDATE
        })
            .then(response => { })
            .catch(error => { console.error('A ocurrido un error al actualizar el estado => ', error); })
            .finally(() => { });
        res.json({
            ok: true,
            msg: 'Se actualizó exitosamente'
        });

        res.json({ idEmployee });
    } catch (error) {
        res.status(500).json(error);
    }
}

const postAddMenusToEmployee = async (req, res) => {
    try {
        const idEmployee = req.params.id;
        const status = req.body.status;
        const idMenu = req.body.idMenu;
        await tblMenus.sequelize.query('CALL spInsertMenuOnEmployeeHasMenu_tickets(:idEmpleado, :idMenu, :status)', {
            replacements: {
                idEmpleado: idEmployee,
                idMenu: idMenu,
                status: status
            },
            type: QueryTypes.INSERT
        })
            .then(response => { })
            .catch(error => { console.error('A ocurrido un error al insertar el estado => ', error); })
            .finally(() => {

            });
        res.json({
            ok: true,
            msg: 'Se insertó exitosamente'
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    getMenusByEmployeeId,
    getMenusAddedEmployee,
    getMenusNotAddedEmployee,
    putMenusStatusActive,
    postAddMenusToEmployee
};