const { QueryTypes, Sequelize } = require('sequelize');
const tblEmpleados = require('../../../Models/Employees/Empleados/employees');
const fields = ['idEmpleado', 'vchNombre', 'vchApellidoPaterno', 'vchApellidoMaterno'];

const getEmpleadosActivos = async (req, res) => {
    try {
        let empleados;
        /* tblEmpleados.findAll({
            attributes: fields,
            where: { blnActivo: true }
        }).then(response => {
            empleados = response;
        }).catch(error => {
            console.error('Error al consultar los registros de los Empleados activos');
        }).finally(() => {
            res.json({ empleados });
        }); */
        tblEmpleados.sequelize.query('CALL spDataEmployeeDisplayOnSelect_tickets()', {
            type: QueryTypes.SELECT
        }).then(result => {
            empleados = result;
        }).catch(error => {
            console.error('Error sp Consultar los empleados del select de solicitar tickets => ', error);
        }).finally(() => {
            res.json({ empleados });
        });
    } catch (error) {
        res.status(500).json(error);
    }
};

const getEmployeeByEmail = async (req, res) => {
    const email = req.params.email;
    try {
        let empleado;
        tblEmpleados.sequelize.query('CALL spSelectEmployeeByEmail_tickets(:email)', {
            replacements: { email: email },
            type: QueryTypes.SELECT
        }).then(result => {
            empleado = result;
        }).catch(error => {
            console.error('Error sp => ', error);
        }).finally(() => {
            res.json({ empleado });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const getEmployeeByTicketId = async (req, res) => {
    try {
        const ticketID = req.params.idTicket;
        let employeeTicket;
        await tblEmpleados.sequelize.query('CALL spSelectEmployeeAttendingByTicketId_tickets(:ticketid)', {
            replacements: { ticketid: ticketID },
            type: QueryTypes.SELECT
        }).then(result => {
            employeeTicket = result;
        }).catch(error => {
            console.error('Error al consultar el error el empleado de este ticket => ', error);
        }).finally(() => {
            res.json({ employeeTicket });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const getEmployeesByAtentionAreaID = async (req, res) => {
    try {
        const atentionAreaID = req.params.areaID;
        const sede = req.params.sede;

        let atentionAreaEmployees;
        await tblEmpleados.sequelize.query('CALL spSelectEmployeesSupportByAtentionAreaID_tickets(:idArea, :sede)', {
            replacements: {
                idArea: atentionAreaID,
                sede: sede
            },
            type: QueryTypes.SELECT
        }).then(result => {
            atentionAreaEmployees = result;
        }).catch(error => {
            console.error('Error al consultar los empleados por area de atencion => ', error);
        }).finally(() => {
            res.json({ atentionAreaEmployees });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const getEmployeesByUserType = async (req, res) => {
    try {
        const tipoUsuarioID = req.params.userTypeID;
        const sede = req.params.sede;
        let employeesByUserType;
        await tblEmpleados.sequelize.query('CALL spSelectEmployeeByUserTypeID_tickets(:tipoUsuarioID, :sede)', {
            replacements: {
                tipoUsuarioID: tipoUsuarioID,
                sede: sede
            },
            type: QueryTypes.SELECT
        }).then(result => {
            employeesByUserType = result;
        }).catch(error => {
            console.error('Error al consultar los empleados por tipo de usuario => ', error);
        }).finally(() => {
            res.json({ employeesByUserType });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const putStatusAccount = async (req, res) => {
    try {
        const empleadoID = req.params.empleadoID;
        const params = req.body;
        await tblEmpleados.sequelize.query('CALL spUpdateAccountStatus_tickets(:idempleado, :status)', {
            replacements: {
                idempleado: empleadoID,
                status: params.status
            }
        }).then(result => {
        }).catch(error => {
            console.error('Error al actualizar el estado de la cuenta de este usuario => ', error);
            res.json({
                success: false
            });
        }).finally(() => {
            res.json({
                success: true
            });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const postNewEmployee = async (req, res) => {
    try {
        const params = req.body;
        let responseResult;
        await tblEmpleados.sequelize.query('CALL spInsertEmployeeHasUserType(:email, :idEmpleado)', {
            replacements: {
                email: params.email,
                idEmpleado: params.idempleado
            },
            type: QueryTypes.INSERT
        }).then(result => {
            responseResult = result;
        }).catch(error => {
            console.error('Error al insertar el registro del nuevo empleado en la bd => ', error);
        }).finally(() => {
            res.json({
                success: true,
                resp: responseResult
            });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const putUserType = async (req, res) => {
    try {
        const empleadoID = req.params.empleadoID;
        const params = req.body;
        await tblEmpleados.sequelize.query('CALL spUpdateUserTypeIDByEmployeeID_tickets(:idempleado, :idtipousuario, :areaAtencion)', {
            replacements: {
                idempleado: empleadoID,
                idtipousuario: params.tipousuario,
                areaAtencion: params.areaAtencion
            }
        }).then(result => {
        }).catch(error => {
            console.error('Error al actualizar el tipo de usuario de este empleado => ', error);
            res.json({
                success: false
            });
        }).finally(() => {
            res.json({
                success: true
            });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    getEmpleadosActivos,
    getEmployeeByEmail,
    getEmployeeByTicketId,
    getEmployeesByAtentionAreaID,
    getEmployeesByUserType,
    putStatusAccount,
    postNewEmployee,
    putUserType
};