const tblTipoUsuarios = require('../Models/userTypes');

// En este array se definen los campos que se quieren consultar a la base de datos
const attributesTipoUsuarios = ['idTipoUsuario', 'vchDescripcion', 'blnActivo'];

// Método para obtener todos los registros de los tipo de usuario que estén activos
const getTipoUsuarios = async (req, res) => {
    try {
        // Variable donde se ha de almacenar la respuesta de la base de datos
        let tipoUsuarios;
        // Método para realizar la búsqueda de los registros segun la cláusua
        tblTipoUsuarios.findAll({
            // Se envian los campos a consultar
            attributes: attributesTipoUsuarios,
            // Se escribe la cláusula si es que se requiere de alguna
            where: { blnActivo: true }
        }).then(response => {
            // Se asigna a la variable local la respuesta emitida por la base de datos
            tipoUsuarios = response;
        }).catch(error => {
            // En caso de que haya error al consultar la base de datos
            console.error('Error al obtener todos los registros de los tipos de usuarios');
        }).finally(() => {
            // Al finalizar la petición se retorna la respuesta del backend para que se muestre como un json
            res.json({ tipoUsuarios });
        });
    } catch (error) {
        // Error para indicar que falló el servidor
        res.status(500).json(error);
    }
}

const getTipoUsuariosById = async (req, res) => {
    const id = req.params.id;
    try {
        let tipoUsuarios;
        tblTipoUsuarios.findAll({
            attributes: attributesTipoUsuarios,
            where: { idTipoUsuario: id }
        }).then(response => {
            tipoUsuarios = response;
        }).catch(error => {
            console.error('Error al obtener los registros de los tipos de usuario por id');
        }).finally(() => {
            res.json({ tipoUsuarios });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    getTipoUsuarios,
    getTipoUsuariosById
};
