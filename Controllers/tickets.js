const tblCreacionTickets = require('../Models/tickets');
const { QueryTypes } = require('sequelize');

const fields = ['idCreacionTicket', 'txtDescripcion', 'fechaSolicitud', 'blnActivo', 'fkIdEstadoTicket', 'fkIdDetalleIncidencia'];

const getTickets = async (req, res) => {
    try {
        let tickets;
        tblCreacionTickets.findAll({
            attributes: fields,
            where: { blnActivo: true }
        }).then(response => {
            tickets = response;
        }).catch(error => {
            console.error('Error al obtener los registros de los Tickets', error);
        }).finally(() => {
            res.json({ tickets });
        });
    } catch (error) {
        res.status(500).json(error);
    }
}

const postTicket = async (req, res) => {
    const params = req.body;
    try {
        const nuevoTicket = await tblCreacionTickets.create({
            txtDescripcion: params.txtDescripcion,
            blnActivo: params.blnActivo,
            fkIdDetalleIncidencia: params.fkIdDetalleIncidencia,
            fkIdEmpleadoSolicita: params.fkIdEmpleadoSolicita,
            fkIdAreaAtencion: params.fkIdAreaAtencion,
            fkIdEstadoTicket: params.fkIdEstadoTicket,
            vchSedeSolicita: params.vchSedeSolicita
        });
        res.json({ nuevoTicket });
    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    getTickets,
    postTicket
};