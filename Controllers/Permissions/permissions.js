const { QueryTypes } = require('sequelize');
const tblEmpleados = require('../../Models/Employees/Empleados/employees');

const getEmployeesSupport = async (req, res) => {
    try {
        const sede = req.params.sede;
        let employeesSupport;
        await tblEmpleados.sequelize.query('CALL spSelectEmployeesSupport_tickets(:sede)', {
            replacements: { sede: sede },
            type: QueryTypes.SELECT
        }).then(response => {
            employeesSupport = response;
        }).catch(error => {
            console.error('Error al consultar los empleados que atienden los tickets => ', error);
        }).finally(() => {
            res.json({ employeesSupport });
        });
    } catch (e) {
        res.status(500).json(e);
    }
}

module.exports = {
    getEmployeesSupport
}