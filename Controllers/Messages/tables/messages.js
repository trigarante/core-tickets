const tblMensajesTickets = require('../../../Models/Messages/tables/messages');
const fields = ['idMensajeTicket', 'fkIdCreacionTicket', 'jsonMensajes'];

const getMensajesById = async (req, res) => {
    const ticketID = req.params.id;
    try {
        let mensajes;
        tblMensajesTickets.findAll({
            attributes: fields,
            where: { fkIdCreacionTicket: ticketID }
        }).then(response => {
            mensajes = response;
        }).catch(error => {
            console.error('Error al consultar los Mensajes del chat', error);
        }).finally(() => {
            console.info('Mensaje base de datos => ', mensajes);
            res.json({ mensajes });
        });
    } catch (error) {
        res.status(500).json(error);
    }
};

const postMensajes = async (req, res) => {
    const params = req.body;
    try {
        const nuevosMensajes = await tblMensajesTickets.create({
            fkIdCreacionTicket: params.ticketID,
            jsonMensajes: params.messages
        });
        res.json({ nuevosMensajes });
    } catch (error) {
        res.status(500).json(error);
    }
}

const putMensajes = async (req, res) => {
    const params = req.body;
    try {
        const mensajeActualizado = await tblMensajesTickets.update({
            jsonMensajes: params.messages,
        }, {
            where: { fkIdCreacionTicket: params.ticketID }
        });
        res.json({ mensajeActualizado });
    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    getMensajesById,
    postMensajes,
    putMensajes
};