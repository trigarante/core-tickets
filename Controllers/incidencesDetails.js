const tblDetalleIncidencias = require('../Models/incidencesDetails');

const fields = ['idDetalleIncidencia', 'vchDescripcion'];

const getDetalleIncidenciasActivas = async (req, res) => {
    try {
        let detalleIncidencias;
        tblDetalleIncidencias.findAll({
            attributes: fields,
            where: { blnActivo: true }
        }).then(response => {
            detalleIncidencias = response;
        }).catch(error => {
            console.error('Error al consultar los Detalles de las Incidencias');
        }).finally(() => {
            res.json({ detalleIncidencias });
        });
    } catch (error) {
        // Error para indicar que falló el servidor
        res.status(500).json(error);
    }
};

const getDetalleIncidenciasActivasById = async (req, res) => {
    const id = req.params.id;
    try {
        let detalleIncidencias;
        tblDetalleIncidencias.findAll({
            attributes: fields,
            where: { blnActivo: true, fkIdIncidencia: id }
        }).then(response => {
            detalleIncidencias = response;
        }).catch(error => {
            console.error('Error al consultar los registros de los Detalles de Incidencias por ID');
        }).finally(() => {
            res.json({ detalleIncidencias });
        });
    } catch (error) {
        // Error para indicar que falló el servidor
        res.status(500).json(error);
    }
}

module.exports = {
    getDetalleIncidenciasActivas,
    getDetalleIncidenciasActivasById
};