const tblSesiones = require('../Models/auth');
const { request, response } = require('express');
const { googleVerify } = require('../Helpers/google-verify');
const { generarJWT } = require('../Helpers/jwt');
const { QueryTypes } = require('sequelize');

const postUsuariosLoginGoogle = async (req, res = response) => {
    try {
        const { token } = req.body;
        const { name, email, picture } = await googleVerify(token);
        const usuarioDB = await tblSesiones.findOne({ where: { email } });
        let usuario;
        if (!usuarioDB) {
            usuario = new tblSesiones({
                nombre: name,
                email,
                accessPass: '@@@',
                img: picture,
                google: true
            });
            await usuario.save();
            const newtoken = await generarJWT(usuario.id);
            res.json({
                ok: true,
                newtoken
            });
        } else {
            let responseStatus;
            await tblSesiones.sequelize.query('CALL spSelectStatusEmployeeByEmail(:correo)', {
                replacements: { correo: email },
                type: QueryTypes.SELECT
            }).then(response => {
                responseStatus = response['0']['0'].blnActivo;
            }).catch(error => {
                console.error('Error al consultar el estado del perfil');
            }).finally(async () => {
                if (responseStatus == 1) {
                    usuario = usuarioDB;
                    usuario.google = true;
                    await usuario.save();
                    const newtoken = await generarJWT(usuario.id);
                    res.json({
                        ok: true,
                        newtoken
                    });
                } else if (responseStatus == 0) {
                    res.json({
                        ok: false,
                        message: 'Usuario bloqueado'
                    });
                } else {
                    res.json({
                        exists: false,
                        message: 'Usuario no existe'
                    });
                }

            });
        }

    } catch (e) {
        console.error('Esto es un error  ', e);
        res.status(500).json(e);
    }
}

const renewToken = async (req, res = response) => {
    const uid = req.uid;
    const token = await generarJWT(uid);
    const usuario = await tblSesiones.findOne(uid);
    res.json({
        ok: true,
        token,
        usuario,
    });
}

const updateSessionActive = async (req, res) => {
    try {
        const email = req.params.email;
        const statusSession = req.body.status;

        const sessionUpdated = await tblSesiones.sequelize
            .query('CALL spUpdateSessionActive_tickets(:email, :status)', {
                replacements: {
                    email: email,
                    status: statusSession
                },
                type: QueryTypes.UPDATE
            }).then(response => { })
            .catch(error => { console.error('Error al actualizar el estado de la sesión => ', error); })
            .finally(() => { });
        res.json({
            ok: true,
            updateSessionActive,
            msg: 'Se actualizó exitosamente'
        });
    } catch (e) {
        res.status(500).json(e);
    }
}

module.exports = {
    postUsuariosLoginGoogle,
    renewToken,
    updateSessionActive
};
