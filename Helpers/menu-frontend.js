const getMenuFrontEnd = (role = 'USER_ROLE') => {
    const menu = [
        {
          titulo: 'Dashboard',
          icono: 'mdi mdi-gauge',
          submenu: [
            { titulo: 'Inicio', url: '/' },
          ]
        },
        {
          titulo: 'Gestión',
          icono: 'mdi mdi-folder-lock-open',
          submenu: [
            // { titulo: 'Usuarios', url: 'usuarios' },
            { titulo: 'Areas', url: 'hospitales' },
            { titulo: 'Encargados', url: 'medicos' },
            { titulo: 'Detalles', url: 'detalles' },
          ]
        },
      ];
    if ( role === 'ADMIN_ROLE' ) {
        menu[1].submenu.unshift({ titulo: 'Usuarios', url: 'usuarios' })
    }
    return menu;
}

module.exports = {
    getMenuFrontEnd
}
