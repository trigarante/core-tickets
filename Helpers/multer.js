'use strict'

const { existsSync, mkdirSync } = require('fs');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const currentFolder = path.join(__dirname, `../Images/Upload/Chat/chat${req.body.chat}`);
        if (!existsSync(currentFolder)) {
            mkdirSync(currentFolder);
        }
        cb(null, currentFolder);
    },
    filename: (req, file, cb) => {
        cb(null, `${req.body.name}.${file.mimetype.split('/')[1]}`);
    }
});

const downloader = (req, res) => {
    const image = path.join(__dirname, `../Images/Upload/Chat/${req.params.chat}/${req.params.image}`)
    res.sendFile(image);
}

module.exports = {
    storage,
    downloader
};